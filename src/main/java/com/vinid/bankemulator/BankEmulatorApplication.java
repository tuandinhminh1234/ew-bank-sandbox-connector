package com.vinid.bankemulator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankEmulatorApplication {

    public static void main(String[] args) {
        System.out.println("starting...");
        SpringApplication.run(BankEmulatorApplication.class, args);
    }

}

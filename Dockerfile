FROM openjdk:8
EXPOSE 8080
ADD target/bank-emulator.jar bank-emulator.jar
ENTRYPOINT ["java", "-jar", "/bank-emulator.jar"]